### Requirements:
- Linux based system, (WSL2 for Windows allowed too);
- Docker - 20.10+

  Installation instructions: https://docs.docker.com/engine/install/

  Check version: `docker version`
- Docker Compose - v2.4+

  Check version: `docker compose version`

  (Optional) If Docker Compose is not installed yet:

  Installation instructions: https://docs.docker.com/compose/install/

### Initial setup:

- ```shell
  docker compose up -d
  ```
- ```shell
  docker compose exec php composer install
  ```
- ```shell
  docker compose exec php drush si --existing-config --account-pass=admin -y
  ```

### Access to frontpage
Site is available at http://d9-test.localhost:8000/ by default. Address may be
changed based on the environment variables configuration.
